package slideshow;

import java.awt.Color;
import java.awt.Graphics;

public class List extends SlideComponent {

    public int radius;

    public List(int x, int y, int radius, Color color) {
        super(x, y, color);
        this.radius = radius;
    }

    @Override
    public void paint(Graphics graphics) {
        super.paint(graphics);
        graphics.drawOval(x, y, x, x);
    }
}
