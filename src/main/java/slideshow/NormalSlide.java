package slideshow;

import java.awt.Color;

public class NormalSlide extends SlideFactory {

    SlideComponent slideComponents = new CompositeSlide();
    int number;

    public NormalSlide(int number) {
        if (number == 2) {
            this.slideComponents = new CompositeSlide(
                    (new Title(40, 50, Color.BLUE, 20, "Slide nummer 2")),
                    new Figure(40, 70, 100, 200, Color.BLUE, true)
            );
        } else if (number == 3) {
            this.slideComponents = new CompositeSlide(
                    (new Title(40, 50, Color.BLUE, 20, "Slide nummer 3")),
                    new Figure(40, 70, 100, 200, Color.BLUE, true)
            );
        }
    }

    @Override
    public SlideComponent getSlideComponents() {
        return this.slideComponents;
    }

}
