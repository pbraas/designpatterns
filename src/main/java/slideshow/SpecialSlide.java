package slideshow;

import java.awt.Color;

public class SpecialSlide extends SlideFactory {

    SlideComponent slideComponents = new CompositeSlide();

    public SpecialSlide() {
        this.slideComponents = new CompositeSlide(
                (new Title(40, 50, Color.BLUE, 20, "Special Slide")),
                new Figure(40, 70, 100, 200, Color.BLUE, true)
        );
    }

    @Override
    public SlideComponent getSlideComponents() {
        return this.slideComponents;
    }


    
    

}
