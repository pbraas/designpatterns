package slideshow;

import abstractfactory2.*;

public class SlideProducer {

    public static AbstractFactory getFactory(boolean special) {
        if (special) {
            return new RoundedShapeFactory();
        } else {
            return new ShapeFactory();
        }
    }
}
