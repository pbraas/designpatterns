package slideshow;

public abstract class  SlideFactory {

    abstract SlideComponent getSlideComponents();
}
