package slideshow;

import java.awt.*;

public class Table extends SlideComponent {

    public int width;
    public int height;

    public Table(int x, int y, int width, int height, Color color) {
        super(x, y, color);
        this.width = width;
        this.height = height;
    }

    @Override
    public void paint(Graphics graphics) {
        super.paint(graphics);
        graphics.drawRect(x, y, width, height);
    }

}
