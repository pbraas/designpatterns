package slideshow;

import java.awt.*;

public class Figure extends SlideComponent {

    private final int width;
    private final int height;
    private boolean figure;

    public Figure(int x, int y, int width, int height, Color color, boolean figure) {
        super(x, y, color);
        this.width = width;
        this.height = height;
        this.figure = figure;
    }
    @Override
    public void paint(Graphics graphics) {
        super.paint(graphics);
        if (this.figure) {
            graphics.drawRect(x, y, width, height);
        } else {
            graphics.drawOval(x, y, width, height);
        }
    }
}
