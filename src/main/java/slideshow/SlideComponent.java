package slideshow;

import java.awt.*;

abstract class SlideComponent implements SlideComponentInterface {

    public int x;
    public int y;
    public Color color;
    private boolean selected = false;

    SlideComponent(int x, int y, Color color) {
        this.x = x;
        this.y = y;
        this.color = color;
    }

    @Override
    public void paint(Graphics graphics) {
        graphics.setColor(Color.BLACK);
    }

}
