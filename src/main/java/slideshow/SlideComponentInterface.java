package slideshow;

import java.awt.*;

public interface SlideComponentInterface {

    void paint(Graphics graphics);
}
