package slideshow;

public class SlideCreator {

    Projector projector;

    public void showSlide(int number) {
        projector = new Projector();
        if (number == 1) {
            projector.loadShapes(new SpecialSlide().getSlideComponents());
        } else {
            projector.loadShapes(new NormalSlide(number).getSlideComponents());
        }
    }
}
