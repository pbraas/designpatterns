package slideshow;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Projector {

    private final Bulb bulb;
    private final CompositeSlide compositeSlide;

    public Projector() {
        compositeSlide = new CompositeSlide();
        bulb = new Bulb();
    }

    public void loadShapes(SlideComponentInterface... slideComponents) {
        compositeSlide.clear();
        compositeSlide.add(slideComponents);
        bulb.turnOnBulb();
    }

    private final class Bulb extends Canvas implements KeyListener {

        private JFrame frame;
        private final int PADDING = 10;
        private final int width = 200;
        private final int height = 300;

        public Bulb() {
            createFrame();
            turnOnBulb();
        }

        public void createFrame() {
            frame = new JFrame();
            frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
            frame.setLocationRelativeTo(null);

            JPanel contentPanel = new JPanel();
            Border padding = BorderFactory.createEmptyBorder(PADDING, PADDING, PADDING, PADDING);
            contentPanel.setBorder(padding);
            frame.setContentPane(contentPanel);

            frame.add(this);
            frame.setVisible(true);
            frame.getContentPane().setBackground(Color.LIGHT_GRAY);
            frame.addKeyListener((KeyListener) this);
        }

        void turnOnBulb() {
            this.setSize(width, height);
            frame.pack();
        }

        @Override
        public void paint(Graphics graphics) {
            compositeSlide.paint(graphics);
        }

        @Override
        public void keyTyped(KeyEvent e) {
            System.out.println(e.getKeyChar());
        }

        @Override
        public void keyPressed(KeyEvent e) {
            System.out.println(e.getKeyChar());
        }

        @Override
        public void keyReleased(KeyEvent e) {
        }
    }
}
