package composite;

public abstract class Graphic {

    protected int x, y;

    public Graphic() {
    }

    public Graphic(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void move(int x, int y) {
        this.x += x;
        this.y += y;
    }

    public abstract void draw();

}
