package composite;

public class Dot extends Graphic {

    public Dot(int x, int y) {
        super(x, y);
    }

    @Override
    public void draw() {
        System.out.println("draw a dot at (" + x + "," + y + ")" );
    }

}
