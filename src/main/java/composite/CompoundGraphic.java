package composite;

import java.util.ArrayList;
import java.util.List;

public class CompoundGraphic extends Graphic {

    List<Graphic> list = new ArrayList<>();

    public void add(Graphic graphic) {
        list.add(graphic);
    }

    public void remove(Graphic graphic) {
        list.remove(graphic);
    }

    @Override
    public void move(int x, int y) {
        list.forEach(g -> g.move(x, y));

    }

    @Override
    public void draw() {
        list.forEach(graphic -> graphic.draw());
    }

}
