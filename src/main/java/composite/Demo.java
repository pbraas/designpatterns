package composite;

public class Demo {

    public static void main(String[] args) {
        CompoundGraphic cg = new CompoundGraphic();
        cg.add(new Circle(5, 6, 7));
        cg.add(new Circle(10, 6, 9));
        cg.add(new Circle(15, 6, 7));
        cg.add(new Dot(1, 2));
        cg.draw();
        cg.move(2, 3);
        cg.draw();

    }

}
