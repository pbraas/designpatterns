package composite;

public class Circle extends Dot {

    private int radius;

    public Circle(int radius, int x, int y) {
        super(x, y);
        this.radius = radius;
    }

    public void draw() {
        System.out.println("draw a circle at (" + x + "," + y + ") with radius " + radius);
    }
}
