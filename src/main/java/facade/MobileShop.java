
package facade;

    public interface MobileShop {  
        public void printModelNumber();  
        public void printPrice();  
    }  
