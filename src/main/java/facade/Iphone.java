package facade;

public class Iphone implements MobileShop {

    @Override
    public void printModelNumber() {
        System.out.println(" Iphone 8 ");
    }

    @Override
    public void printPrice() {
        System.out.println(" €900,- ");
    }
}
