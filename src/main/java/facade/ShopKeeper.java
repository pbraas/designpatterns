package facade;

public class ShopKeeper {

    private MobileShop iphone;
    private MobileShop samsung;
    private MobileShop blackberry;

    public ShopKeeper() {
        iphone = new Iphone();
        samsung = new Samsung();
        blackberry = new Blackberry();
    }

    public void saleIphone() {
        iphone.printModelNumber();
        iphone.printPrice();
    }

    public void saleSamsung() {
        samsung.printModelNumber();
        samsung.printPrice();
    }

    public void saleBlackBerry() {
        blackberry.printModelNumber();
        blackberry.printPrice();
    }
}
