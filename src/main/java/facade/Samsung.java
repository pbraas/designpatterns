package facade;

public class Samsung implements MobileShop {

    @Override
    public void printModelNumber() {
        System.out.println(" Samsung galaxy tab 3 ");
    }

    @Override
    public void printPrice() {
        System.out.println(" €250,- ");
    }
}
