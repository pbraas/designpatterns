package facade;

public class Blackberry implements MobileShop {

    @Override
    public void printModelNumber() {
        System.out.println(" Blackberry Z10 ");
    }

    @Override
    public void printPrice() {
        System.out.println(" €750,- ");
    }
}
