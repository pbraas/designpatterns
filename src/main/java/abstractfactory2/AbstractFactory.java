package abstractfactory2;

public abstract class AbstractFactory {

    abstract Shape getShape(String shapeType);
}
