package abstractfactory2;

public class Demo {

    public static void main(String[] args) {
        //get shape factory
        AbstractFactory shapeFactory = FactoryProducer.getFactory(false);
        //get an object of Shape Rectangle
        Shape rectangle = shapeFactory.getShape("RECTANGLE");
        //call draw method of Shape Rectangle
        rectangle.draw();
        //get an object of Shape Square 
        Shape square = shapeFactory.getShape("SQUARE");
        //call draw method of Shape Square
        square.draw();
        //get shape factory
        AbstractFactory shapeFactory1 = FactoryProducer.getFactory(true);
        //get an object of Shape Rectangle
        Shape RoundedRectangle = shapeFactory1.getShape("RECTANGLE");
        //call draw method of Shape Rectangle
        RoundedRectangle.draw();
        //get an object of Shape Square 
        Shape RoundedSquare = shapeFactory1.getShape("SQUARE");
        //call draw method of Shape Square
        RoundedSquare.draw();

    }
}
