package decorator;

public class Table extends SlideDecorator {

    public Table(Slide slide) {
        super(slide);
    }

    public Table() {
    }
    

    public String addTable() {
        populateTable();
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Famous athletes \n");
        for (Object[] row : populateTable()) {
            stringBuilder.append("");
            for (Object row1 : row) {
                stringBuilder.append(row1).append("|");
            }
            stringBuilder.append("\n");
        }
        stringBuilder.append("\n\n");
        return stringBuilder.toString();
    }

    @Override
    public String getSlide() {
        return slide.getSlide() + addTable();
    }

    private Object[][] populateTable() {
        Object[][] data = {
            {"Firstname", "Lastname", "sport", "Olympic gold"},
            {"Muhammed", "Ali", "boxing", true},
            {"Serena ", "Williams", "tennis", false},
            {"Husain", "Bolt", "sprint", true},
            {"Gerrie", "Kneteman", "cycling", false},
            {"Alex", "Morgan", "soccer", true}
        };

        return data;
    }

}
