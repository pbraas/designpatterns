package decorator;

abstract class SlideDecorator implements Slide {

    protected Slide slide;

    public SlideDecorator(Slide slide) {
        this.slide = slide;
    }
    
    public SlideDecorator() {
        this.slide=() -> "Using the special slide \n";
    }

}
