package decorator;

public class SpecialSlide implements Slide {

    @Override
    public String getSlide() {
        //table is obligated in a special slide
        Slide slide = new Table();
        return slide.getSlide();
    }

}
