package decorator;

public class NormalSlide implements Slide {

    @Override
    public String getSlide() {
        return "Title of a normal slide \n";
    }

}
