package decorator;

public class EmptySlide implements Slide {

    @Override
    public String getSlide() {
        return "";
    }

}
