package decorator;

public class Title extends SlideDecorator {

    public Title(Slide slide) {
        super(slide);
    }

    @Override
    public String getSlide() {
        return slide.getSlide()+ addTitle();
    }

    private String addTitle() {
        return "Most famous athletes in 2020 \n";
    }
}
