package decorator;

import java.util.Iterator;
import java.util.LinkedList;

public class List extends SlideDecorator {

    LinkedList<String> list = new LinkedList();

    public List(Slide slide) {
        super(slide);
    }

    @Override
    public String getSlide() {
        populateList();
        StringBuilder stringBuilder = new StringBuilder();
        Iterator iterator = list.iterator();
        stringBuilder.append("List of most famous sporters \n");
        while (iterator.hasNext()) {
            stringBuilder.append(iterator.next());
            stringBuilder.append(" \n");
        }
        return slide.getSlide()+stringBuilder.toString();
    }

    private void populateList() {

        list.add("1. Football/Soccer");
        list.add("2. Basketball");
        list.add("3. Cricket");
        list.add("4. Tennis");
        list.add("5. Athletics");
    }

}
